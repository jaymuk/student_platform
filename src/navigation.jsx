import {Link} from 'react-router-dom'

const mainStyle = {
  minHeight: 500
}

const NavigationSidebar = () => {
  return (
    <div
      style={mainStyle}>
      <div>
        STUDENT PLATFORM
      </div>
      <div>
        <Link to='/students'>
          STUDENTS
        </Link>
      </div>
      <div>
        <Link to='/groups'>
          GROUPS
        </Link>
      </div>
    </div>
  )
}

export default NavigationSidebar
