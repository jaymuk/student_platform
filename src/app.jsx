import Layout from './layout'
import {hot} from 'react-hot-loader'

const App = ({db}) => {
  return (
      <Layout db={db} />
  )
}

export default hot(module)(App)
