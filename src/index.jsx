require('bootstrap/dist/css/bootstrap.min.css')
import 'react-select/dist/react-select.css'
const firebase = require("firebase")
require("firebase/firestore")

import React from 'react'
import {render} from 'react-dom'
import {BrowserRouter} from 'react-router-dom'
import App from './app'

const config = {
  apiKey: "AIzaSyDMhNBKR7m1EcFBNtS7VPDFVfURjwaIzFI",
  authDomain: "student-platform-934be.firebaseapp.com",
  databaseURL: "https://student-platform-934be.firebaseio.com",
  projectId: "student-platform-934be",
  storageBucket: "student-platform-934be.appspot.com",
  messagingSenderId: "1000469216668"
}

firebase.initializeApp(config)

export const db = firebase.firestore()

render(
  <BrowserRouter>
    <App db={db}/>
  </BrowserRouter>
, document.getElementById('app'))


