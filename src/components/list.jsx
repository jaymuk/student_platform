import AddStudentModal from './add_student_modal'
import Select from 'react-select'
import {orderBy} from 'lodash'

class List extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      students: [],
      groups: [],
      addingStudentToGroup: false,
      sortBy: 'a-z'
    }

    this.handleAddStudentToGroup = this.handleAddStudentToGroup.bind(this)


    this.props.db.collection('students').onSnapshot((querySnapshot) => {
      const students = querySnapshot.docs.map(d => d.data())

      this.setState(s => ({...s, students}))
    })

    this.props.db.collection('groups').onSnapshot((querySnapshot) => {
      const groups = querySnapshot.docs.map(d => d.data())

      this.setState(s => ({...s, groups}))
    })

    this.props.db.collection('students').get().then((querySnapshot) => {
      const students = querySnapshot.docs.map(d => d.data())

      this.setState(s => ({...s, students}))
    })

    const {location: {pathname}} = this.props

    if (pathname === '/groups') {
      this.props.db.collection('groups').get().then((querySnapshot) => {
        const groups = querySnapshot.docs.map(d => d.data())

        this.setState(s => ({...s, groups}))
      })
    }
  }

  openModal = () => {
    this.setState((s) => ({...s, addingStudentToGroup: true}))
  }

  closeModal = () => {
    this.setState((s) => ({...s, addingStudentToGroup: false}))
  }

  handleAddStudentToGroup = ({groupName: name, student}) => {
    const {firstName, lastName} = student,
      studentKey = firstName + lastName

    this.props.db.collection('groups').doc(name).collection('students').doc(studentKey).set({
     student
    }, {merge: true})
  }

  handleSortByChange = ({value}) => {
    this.setState((s) => ({...s, sortBy: value}))
  }

  header() {
    const {location: {pathname}} = this.props

    const AddStudentButton = () => {
      return (
        <div>
          <div
            className='btn btn-light'
            onClick={this.openModal}>
            Add student to group
          </div>
          <AddStudentModal
            groups={this.state.groups}
            students={this.state.students}
            isOpen={this.state.addingStudentToGroup}
            handleAddStudentToGroup={this.handleAddStudentToGroup}
            onCloseModal={this.closeModal} />
        </div>
      )
    }

    return (
      <div>
        {pathname === '/groups' && <AddStudentButton />}
        <div className='pull-right'>
          <Select
            placeholder='Sort by'
            value={{label: this.state.sortBy}}
            onChange={this.handleSortByChange}
            options={['a-z', 'z-a', 'id-asc', 'id-desc'].map((o) => ({value: o, label: o}))}
            />
        </div>
      </div>
    )
  }

  studentList = () => {
    const {students, sortBy} = this.state

    let orderedStudents

    switch (sortBy) {
      case 'a-z':
        orderedStudents = orderBy(students, ['firstName', 'lastName'], ['asc', 'asc'])
        break
      case 'z-a':
        orderedStudents = orderBy(students, ['firstName', 'lastName'], ['desc', 'desc'])
        break
      case 'id-asc':
        orderedStudents = orderBy(students, ['id'], ['asc'])
        break
      case 'id-desc':
        orderedStudents = orderBy(students, ['id'], ['desc'])
        break
      default:
        orderedStudents = orderBy(students, ['firstName', 'lastName'], ['asc', 'asc'])
    }

    return (
      <ol>
        {orderedStudents.map(s => <li>{`${s.firstName} ${s.lastName}`}</li>)}
      </ol>
    )

  }

  render() {
    return (
      <div>
        {this.header()}
        {this.studentList()}
      </div>
    )
  }
}

export default List
