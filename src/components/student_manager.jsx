const Header = () => {
  return (
    <div className='col-md-6'>
      Manage stdent groups
    </div>
  )
}

class StudentManager extends React.Component {
  constructor(props) {
    super(props)

    this.state = {firstName: '', lastName: ''}
  }

  handleFirstNameChange = (event) => {
    event.preventDefault()
    const {value} = event.target
    this.setState(s => ({...s, firstName: value}))
  }

  handleLastNameChange = (event) => {
    event.preventDefault()
    const {value} = event.target
    this.setState(s => ({...s, lastName: value}))
  }

  handleSubmit = (event) => {
    event.preventDefault()
    event.persist()
    this.props.db.collection("students").add({
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      id: firebase.firestore.Timestamp.now().seconds
    }).then(() => this.setState({firstName: '', lastName: ''}))
  }

  render() {
    return (
      <div onSubmit={this.handleSubmit}>
        <p>Add new student</p>
        <form>
          <label>
            Student First Name:
            <input
              type="text"
              value={this.state.firstName}
              onChange={this.handleFirstNameChange}/>
          </label>
          <label>
            Student Last Name:
            <input
              type="text"
              value={this.state.lastName}
              onChange={this.handleLastNameChange}/>
          </label>
          <input type='submit' value='Submit' />
        </form>
      </div>
    )
  }
}

export default StudentManager
