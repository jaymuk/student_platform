import Select from 'react-select'
import Modal from 'react-modal'
import {db} from '../index'
import {differenceBy} from 'lodash'

const modalStyle = {
  overlay: {
    zIndex: 1069,
    backgroundColor: 'rgba(38, 113, 171, 0.4)',
    overflow: 'scroll'
  },
  content: {
    top: 30,
    bottom: 'auto',
    left: '50%',
    right: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, 0)',
    padding: 30,
    overflow: 'visible',
    position: 'absolute',
    minWidth: 400
  }
}

const transformGroup = (g) => {
  return {value: g.name, label: g.name, source: g}
}

const transformStudent = (s) => {
  return {
    value: `${s.firstName} ${s.lastName}`,
    label: `${s.firstName} ${s.lastName}`,
    source: s
  }
}

class AddStudentModal extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedGroup: null,
      selectedGroupStudents: null,
      selectedStudent: null
    }
  }

  handleGroupChange = ({source: selectedGroup}) => {
    this.setState(s => ({...s, selectedGroup}))

    db.collection('groups').doc(selectedGroup.name).collection('students').get().then((querySnapshot) => {
      const selectedGroupStudents = querySnapshot.docs.map(d => d.data().student)

      this.setState(s => ({...s, selectedGroupStudents}))
    })
  }

  handleStudentChange = ({source: selectedStudent}) => {
    this.setState(s => ({...s, selectedStudent}))
  }

  selectors() {
    const {selectedGroup, selectedStudent, selectedGroupStudents} = this.state,
      {groups, students} = this.props,
      studentsNotInGroup = differenceBy(students, selectedGroupStudents, 'id')

    return (
      <div>
        <Select
          value={selectedGroup && transformGroup(selectedGroup)}
          placeholder={'Choose a group'}
          onChange={this.handleGroupChange}
          options={groups.map(transformGroup)} />
        <Select
          value={selectedStudent && transformStudent(selectedStudent)}
          placeholder={'Choose a student'}
          onChange={this.handleStudentChange}
          options={studentsNotInGroup.map(transformStudent)} />
      </div>
    )
  }

  handleAddStudent = () => {
    const {selectedGroup: {name: groupName}, selectedStudent: student} = this.state
    this.props.handleAddStudentToGroup({groupName, student})

    this.props.onCloseModal()
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        onRequestClose={this.props.onCloseModal}
        style={modalStyle}
        shouldCloseOnOverlayClick={true}
        contentLabel='Add student to a group'>
        {this.selectors()}
        <button onClick={this.props.onCloseModal}>Cancel</button>
        <button onClick={this.handleAddStudent}>Add</button>
      </Modal>
    )
  }
}

export default AddStudentModal
