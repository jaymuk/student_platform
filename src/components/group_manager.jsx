import Select from 'react-select'
import {map, orderBy} from 'lodash'
import {db} from '../index'

class Group extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      students: []
    }

    db.collection('groups').doc(this.props.group.name).collection('students').get().then((querySnapshot) => {
      const students =  querySnapshot.docs.map(d => d.data().student)

      this.setState(s => ({...s, students}))
    })

    db.collection('groups').doc(this.props.group.name).collection('students').onSnapshot((querySnapshot) => {
      const students = querySnapshot.docs.map(d => d.data().student)

      this.setState(s => ({...s, students}))
    })
  }

  removeStudentFromGroup = ({student}) => {
    const {firstName, lastName} = student,
      studentKey = firstName + lastName,
      {group: {name}} = this.props

    db.collection('groups').doc(name).collection('students').doc(studentKey).delete().then(() => {
      console.log('student removed')
    }).catch(() => {
      console.log('error removing student from group')
    })
  }

  deleteGroup = (name) => {
    db.collection('groups').doc(name).delete().then(() => {
      console.log('group deleted')
    }).catch(() => {
      console.log('error deleting group')
    })
  }

  listItem = (student) => {
    return (
      <li>
        {`${student.firstName} ${student.lastName}`}
        <div onClick={() => this.removeStudentFromGroup({student})}>
          x
        </div>
      </li>
    )
  }

  render() {
    const {students} = this.state,
      {group: {name}} = this.props

    return (
      <div className='col-md-6'>
        <p>{name}</p>
        <p onClick={() => this.deleteGroup(name)}>Delete Group</p>
        Students:
        <ol>
          {map(students, this.listItem)}
        </ol>
      </div>
    )
  }
}

class GroupManager extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      addingGroup: false,
      groups: [],
      sortBy: 'a-z'
    }

    this.props.db.collection('groups').onSnapshot((querySnapshot) => {
      const groups = querySnapshot.docs.map(d => d.data())

      this.setState(s => ({...s, groups}))
    })
  }

  handleGroupNameChange = (event) => {
    event.preventDefault()
    const {value} = event.target
    this.setState(s => ({...s, name: value}))
  }

  handleAddNewGroup = (event) => {
    const {name} = this.state

    event.preventDefault()
    event.persist()
    this.props.db.collection("groups").doc(name).set({
      name,
      id: firebase.firestore.Timestamp.now().seconds
    }, {merge: true}).then(() => this.setState({addingGroup: false, name: ''}))
  }

  handleStartAdding = () => {
    this.setState((s) => ({...s, addingGroup: true}))
  }

  addGroupComponent = () => {
    if (this.state.addingGroup) {
      return (
        <form onSubmit={this.handleAddNewGroup}>
          <label>
            Group Name:
            <input
              type="text"
              value={this.state.name}
              onChange={this.handleGroupNameChange} />
          </label>
          <input type='submit' value='Add Group' />
        </form>
      )
    }

    return (
    <div
      className='col-md-6'
      onClick={this.handleStartAdding}>
      Add new group
    </div>
    )
  }

  orderedGroups = () => {
    const {groups, sortBy} = this.state

    let orderedGroups

    switch (sortBy) {
      case 'a-z':
        orderedGroups = orderBy(groups, ['name'], ['asc'])
        break
      case 'z-a':
        orderedGroups = orderBy(groups, ['name'], ['desc'])
        break
      case 'id-asc':
        orderedGroups = orderBy(groups, ['id'], ['asc'])
        break
      case 'id-desc':
        orderedGroups = orderBy(groups, ['id'], ['desc'])
        break
      default:
        orderedGroups = orderBy(groups, ['name'], ['asc'])
    }

    return orderedGroups
  }

  handleSortByChange = ({value}) => {
    this.setState((s) => ({...s, sortBy: value}))
  }

  header = () => {
    const {location: {pathname}} = this.props

    return (
      <div>
        {pathname === '/groups' && 'Manage student groups'}
        <div className='pull-right'>
          <Select
            placeholder='Sort by'
            value={{label: this.state.sortBy}}
            onChange={this.handleSortByChange}
            options={['a-z', 'z-a', 'id-asc', 'id-desc'].map((o) => ({value: o, label: o}))}
            />
        </div>
      </div>
    )
  }

  render() {
    return (
      <div>
      {this.header()}
      {this.orderedGroups().map(
        group => <Group group={group} />
      )}
        {this.addGroupComponent()}
      </div>
    )
  }
}

export default GroupManager
