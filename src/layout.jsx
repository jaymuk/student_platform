import {Route, Switch} from 'react-router-dom'
import NavigationSidebar from './navigation'
import StudentManager from './components/student_manager'
import GroupManager from './components/group_manager'
import List from './components/list'

const Layout = ({db}) => {
  return (
    <div className='row container'>
      <div className='col-md-2'>
        <NavigationSidebar />
      </div>
      <div className='col-md-6'>
        <Switch>
          <Route
            path='/students'
            render={(props) => <StudentManager db={db} {...props} />} />
          <Route
            path='/groups'
            render={(props) => <GroupManager db={db} {...props} />} />
          <Route
            render={(props) => <StudentManager db={db} {...props} />} />
        </Switch>
      </div>
      <div className='col-md-4'>
        <Switch>
          <Route
            path='/students'
            render={(props) => <List db={db} {...props} />} />
          <Route
            path='/groups'
            render={(props) => <List db={db} {...props} />} />
          <Route
            render={(props) => <List db={db} {...props} />} />
        </Switch>
      </div>
    </div>
  )
}

export default Layout
